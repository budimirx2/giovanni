import React from 'react'
import PropTypes from 'prop-types'
import { TourTemplate } from '../../templates/tour-page'

const TourPreview = ({ entry, widgetFor }) => (
  <TourTemplate
    image={entry.getIn(['data', 'image'])}
    title={entry.getIn(['data', 'title'])}
    content={widgetFor('body')}
  />
)

TourPreview.propTypes = {
  entry: PropTypes.shape({
    getIn: PropTypes.func,
  }),
  widgetFor: PropTypes.func,
}

export default TourPreview
