import React from 'react'
// import { Link } from 'gatsby'

// import logo from '../img/logo.svg'
import facebook from '../img/facebook.svg'
import instagram from '../img/instagram.svg'
import soundcloud from '../img/soundcloud.svg'
import mail from '../img/gmail.svg'

const Footer = class extends React.Component {
  render() {
    return (
      <footer
        className="footer has-background-black has-text-white-ter"
        style={{ padding: '2em 1em 3em' }}
      >
        <div className="content has-text-centered">
          <h1>Giovanni Chirico</h1>
          <small style={{ color: '#666' }}>
            &copy; 2019 All rights reserved by Giovanni Chirico
          </small>
        </div>
        <div className="content has-text-centered has-background-black has-text-white-ter">
          <div className="container has-background-black has-text-white-ter">
            <div className="columns">
              {/* <div className="column is-4">
                <section className="menu">
                  <ul className="menu-list">
                    <li>
                      <Link to="/" className="navbar-item">
                        Home
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/about">
                        About
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/products">
                        Products
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/contact/examples">
                        Form Examples
                      </Link>
                    </li>
                    <li>
                      <a
                        className="navbar-item"
                        href="/admin/"
                        target="_blank"
                        rel="noopener noreferrer"
                      >
                        Admin
                      </a>
                    </li>
                  </ul>
                </section>
              </div>
              <div className="column is-4">
                <section>
                  <ul className="menu-list">
                    <li>
                      <Link className="navbar-item" to="/blog">
                        Latest Stories
                      </Link>
                    </li>
                    <li>
                      <Link className="navbar-item" to="/contact">
                        Contact
                      </Link>
                    </li>
                  </ul>
                </section>
              </div> */}
              <div className="column is-12 social">
                <a title="facebook" href="https://www.facebook.com/gio.chi.sax/">
                  <img
                    src={facebook}
                    alt="Facebook"
                    style={{ width: '2em', height: '2em' }}
                  />
                </a>
                <a title="soundcloud" href="https://soundcloud.com/giovanni-chirico-422713521">
                  <img
                    className="fas fa-lg"
                    src={soundcloud}
                    alt="Soundcloud"
                    style={{ width: '2em', height: '2em' }}
                  />
                </a>
                <a title="instagram" href="https://www.instagram.com/giovannichi.sax/">
                  <img
                    src={instagram}
                    alt="Instagram"
                    style={{ width: '2em', height: '2em' }}
                  />
                </a>
                <a title="gmail" href="mailto:giovannichi.sax@gmail.com">
                  <img
                    src={mail}
                    alt="Gmail"
                    style={{ width: '2em', height: '2em' }}
                  />
                </a>
              </div>
            </div>
          </div>
        </div>
      </footer>
    )
  }
}

export default Footer
