import React, { Component } from 'react';
// import ReactBnbGallery from 'react-bnb-gallery';
import Gallery from 'react-grid-gallery';

// Photo imports
import Photo01 from '../../static/img/GiovanniChirico-01.jpg'
// import Photo02 from '../../static/img/GiovanniChirico-02.jpg'
import Photo03 from '../../static/img/GiovanniChirico-03.jpg'
import Photo04 from '../../static/img/GiovanniChirico-04.jpg'
import Photo05 from '../../static/img/GiovanniChirico-05.jpg'
import Photo06 from '../../static/img/GiovanniChirico-06.jpg'
import Photo07 from '../../static/img/GiovanniChirico-07.jpg'
import Photo08 from '../../static/img/GiovanniChirico-08.jpg'
import Photo09 from '../../static/img/GiovanniChirico-09.jpg'
import Photo10 from '../../static/img/GiovanniChirico-10.jpg'
import Photo11 from '../../static/img/GiovanniChirico-11.jpg'
import Photo12 from '../../static/img/GiovanniChirico-12.jpg'

const photos = [
  {
    src: Photo11,
    thumbnail: Photo11,
  },
  {
    src: Photo08,
    thumbnail: Photo08,
  },
  {
    src: Photo10,
    thumbnail: Photo10,
  },
  {
    src: Photo07,
    thumbnail: Photo07,
  },
  {
    src: Photo03,
    thumbnail: Photo03,
  },
  {
    src: Photo06,
    thumbnail: Photo06,
  },
  // {
  //   src: Photo02,
  //   thumbnail: Photo02,
  // },
  {
    src: Photo04,
    thumbnail: Photo04,
  },
  {
    src: Photo09,
    thumbnail: Photo09,
  },
  {
    src: Photo12,
    thumbnail: Photo12,
  },
  {
    src: Photo01,
    thumbnail: Photo01,
  },
  {
    src: Photo05,
    thumbnail: Photo05,
  },
];

class GalleryContainer extends Component {
  // constructor() {
  //   super(...arguments);
  //   this.state = {
  //     galleryOpened: false
  //   };
  //   this.toggleGallery = this.toggleGallery.bind(this);
  // }

  // toggleGallery() {
  //   this.setState(prevState => ({
  //     galleryOpened: !prevState.galleryOpened
  //   }));
  // }

  render () {
    return (
      <Gallery
        images={photos}
        enableImageSelection={false}
        margin={0}
      />
    )
    // return (
    //   <>
    //     <button onClick={this.toggleGallery}>
    //       Open gallery
    //     </button>
    //     <ReactBnbGallery
    //       show={this.state.galleryOpened}
    //       photos={photos}
    //       onClose={this.toggleGallery}
    //     />
    //   </>
    // )
  }
}

export default GalleryContainer;