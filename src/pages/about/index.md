---
templateKey: 'about-page'
path: /about
image: /img/GiovanniChirico-07.jpg
something: Something
title: Who is Giovanni Chirico?!
# lang:
#   - italian: >
#     We sell green and roasted coffee beans that are sourced directly from
#     independent farmers and farm cooperatives. We’re proud to offer a
#     variety of coffee beans grown with great care for the environment and
#     local communities. Check our post or contact us directly for current
#     availability.
#   - english: >
#     I discover the saxophone when I was teenager, and as the Southern Italian tradition dictates, I start to play in the old wind orchestra of my town, San Michele Salentino.

#     The love for music and for the saxophone bring me to apply at Conservatoire “Nino Rota”, Monopoli, where I take the Diploma in Saxophone with highest rank.

#     During the studies I approach to the non-classical music, starting with rock, and then jazz, ska, reggae, funk. I have a passion for ethnic music, especially of the Mediterranean, and I found my first band, Kaìlia, with whom I do concerts all over Italy and with whom I record an EP.

#     After classical studies, I meet Roberto Ottaviano by chance. I see his concerts. The beauty of his music convinces me to ask to study with him. He brings me to Conservatoire “Niccolò Piccinni”, Bari, and under his guide I take a master degree in Jazz Saxophone with highest rank plus laude.

#     I’m active in various music ambients and I start the first national and international collaborations.

#     I follow several workshops with artist from all the world, for exemple Joe Lovano, Enrico Rava, Javier Girotto, Stephane Galland, Fabrizio Cassol, Marshall Allen, Bob Stoloff, Amir Elsaffar, Ahmad Al-Qatib, Zied Zouari, Emanuele Cisi, Dado Moroni, Bebo Ferra, Fulvio Sigurtà, Juan Jimenez Alba, and many others.

#     I want to meet musicians, I apply for various international contests of classical musica and jazz music, and I win them. Locomotive Jam Festival trusts in me and gives me a scholarship to attend the Nuoro Jazz courses. In Nuoro someone tell me about an artistic residency in Aix-en-Provence, France, for musicians from all over the Mediterranean. I apply, I pass the audition, I attend the first artistic residency, I become an artist of Medinea Network, and I attend other sessions organized by the Aix-en-Provence Festival, with the aim for me to be part of a more and more connected Mediterranean. Right there I have the chance to meet great artists and with whom I’m in touch for the next projects, particularly to do a stable collective.

#     In 2018 I decide to spend some months in Berlin. I go to all the jam sessions of all the clubs of all the city. I move always with my baritone sax on the shoulders, hungry of beauty. I meet great musicians and I’m in touch with Alex’s Hand, a band who melt jazz, contemporary rock and experimental music, composed by musicians from America, Europe, Asia. After one week I record with them the album “Hungarian Spa” and I’ll play with them in the European Tour 2019.

#     I appear in various albums as saxophone player, composer, arranger, director, for example: Ipcress Files - Roberto Ottaviano (special guest Giovanni Falzone), Vostok Project, Kaìlia, BandAdriatica - Odissea, Alex Semprevivo - Art of the Messengers, Alex’s Hand - Hungarian Spa, La Répétition - Orchestra Senza Confini.

#     In 2019 I decide to leave my job as a teacher to concentrate only on myself, and I record the first album in my name: BASES, in trio with my “brothers”, with whom I started to play music as a professional when my career begun.
---
<!-- ### Shade-grown coffee
Coffee is a small tree or shrub that grows in the forest understory in its wild form, and traditionally was grown commercially under other trees that provided shade. The forest-like structure of shade coffee farms provides habitat for a great number of migratory and resident species.

### Single origin
Single-origin coffee is coffee grown within a single known geographic origin. Sometimes, this is a single farm or a specific collection of beans from a single country. The name of the coffee is then usually the place it was grown to whatever degree available.

### Sustainable farming
Sustainable agriculture is farming in sustainable ways based on an understanding of ecosystem services, the study of relationships between organisms and their environment. What grows where and how it is grown are a matter of choice and careful consideration for nature and communities.

### Direct sourcing
Direct trade is a form of sourcing practiced by some coffee roasters. Advocates of direct trade practices promote direct communication and price negotiation between buyer and farmer, along with systems that encourage and incentivize quality.

### Reinvest profits
We want to truly empower the communities that bring amazing coffee to you. That’s why we reinvest 20% of our profits into farms, local businesses and schools everywhere our coffee is grown. You can see the communities grow and learn more about coffee farming on our blog. -->

<div class="columns">
  <div class="column is-6">
    <strong>ENGLISH</strong>
    I discover the saxophone when I was teenager, and as the Southern Italian tradition dictates, I start to play in the old wind orchestra of my town, San Michele Salentino.
    
    The love for music and for the saxophone bring me to apply at Conservatoire “Nino Rota”, Monopoli, where I take the Diploma in Saxophone with highest rank.
    
    During the studies I approach to the non-classical music, starting with rock, and then jazz, ska, reggae, funk. I have a passion for ethnic music, especially of the Mediterranean, and I found my first band, Kaìlia, with whom I do concerts all over Italy and with whom I record an EP.
    
    After classical studies, I meet Roberto Ottaviano by chance. I see his concerts. The beauty of his music convinces me to ask to study with him. He brings me to Conservatoire “Niccolò Piccinni”, Bari, and under his guide I take a master degree in Jazz Saxophone with highest rank plus laude.
    
    I’m active in various music ambients and I start the first national and international collaborations.
    
    I follow several workshops with artist from all the world, for exemple Joe Lovano, Enrico Rava, Javier Girotto, Stephane Galland, Fabrizio Cassol, Marshall Allen, Bob Stoloff, Amir Elsaffar, Ahmad Al-Qatib, Zied Zouari, Emanuele Cisi, Dado Moroni, Bebo Ferra, Fulvio Sigurtà, Juan Jimenez Alba, and many others.
    
    I want to meet musicians, I apply for various international contests of classical musica and jazz music, and I win them. Locomotive Jam Festival trusts in me and gives me a scholarship to attend the Nuoro Jazz courses. In Nuoro someone tell me about an artistic residency in Aix-en-Provence, France, for musicians from all over the Mediterranean. I apply, I pass the audition, I attend the first artistic residency, I become an artist of Medinea Network, and I attend other sessions organized by the Aix-en-Provence Festival, with the aim for me to be part of a more and more connected Mediterranean. Right there I have the chance to meet great artists and with whom I’m in touch for the next projects, particularly to do a stable collective.
    
    In 2018 I decide to spend some months in Berlin. I go to all the jam sessions of all the clubs of all the city. I move always with my baritone sax on the shoulders, hungry of beauty. I meet great musicians and I’m in touch with Alex’s Hand, a band who melt jazz, contemporary rock and experimental music, composed by musicians from America, Europe, Asia. After one week I record with them the album “Hungarian Spa” and I’ll play with them in the European Tour 2019.
    
    I appear in various albums as saxophone player, composer, arranger, director, for example: Ipcress Files - Roberto Ottaviano (special guest Giovanni Falzone), Vostok Project, Kaìlia, BandAdriatica - Odissea, Alex Semprevivo - Art of the Messengers, Alex’s Hand - Hungarian Spa, La Répétition - Orchestra Senza Confini.
    
    In 2019 I decide to leave my job as a teacher to concentrate only on myself, and I record the first album in my name: BASES, in trio with my “brothers”, with whom I started to play music as a professional when my career begun.
  </div>

  <div class="column is-6">
    <strong>ITALIAN</strong>
    I discover the saxophone when I was teenager, and as the Southern Italian tradition dictates, I start to play in the old wind orchestra of my town, San Michele Salentino.
    
    The love for music and for the saxophone bring me to apply at Conservatoire “Nino Rota”, Monopoli, where I take the Diploma in Saxophone with highest rank.
    
    During the studies I approach to the non-classical music, starting with rock, and then jazz, ska, reggae, funk. I have a passion for ethnic music, especially of the Mediterranean, and I found my first band, Kaìlia, with whom I do concerts all over Italy and with whom I record an EP.
    
    After classical studies, I meet Roberto Ottaviano by chance. I see his concerts. The beauty of his music convinces me to ask to study with him. He brings me to Conservatoire “Niccolò Piccinni”, Bari, and under his guide I take a master degree in Jazz Saxophone with highest rank plus laude.
    
    I’m active in various music ambients and I start the first national and international collaborations.
    
    I follow several workshops with artist from all the world, for exemple Joe Lovano, Enrico Rava, Javier Girotto, Stephane Galland, Fabrizio Cassol, Marshall Allen, Bob Stoloff, Amir Elsaffar, Ahmad Al-Qatib, Zied Zouari, Emanuele Cisi, Dado Moroni, Bebo Ferra, Fulvio Sigurtà, Juan Jimenez Alba, and many others.
    
    I want to meet musicians, I apply for various international contests of classical musica and jazz music, and I win them. Locomotive Jam Festival trusts in me and gives me a scholarship to attend the Nuoro Jazz courses. In Nuoro someone tell me about an artistic residency in Aix-en-Provence, France, for musicians from all over the Mediterranean. I apply, I pass the audition, I attend the first artistic residency, I become an artist of Medinea Network, and I attend other sessions organized by the Aix-en-Provence Festival, with the aim for me to be part of a more and more connected Mediterranean. Right there I have the chance to meet great artists and with whom I’m in touch for the next projects, particularly to do a stable collective.
    
    In 2018 I decide to spend some months in Berlin. I go to all the jam sessions of all the clubs of all the city. I move always with my baritone sax on the shoulders, hungry of beauty. I meet great musicians and I’m in touch with Alex’s Hand, a band who melt jazz, contemporary rock and experimental music, composed by musicians from America, Europe, Asia. After one week I record with them the album “Hungarian Spa” and I’ll play with them in the European Tour 2019.
    
    I appear in various albums as saxophone player, composer, arranger, director, for example: Ipcress Files - Roberto Ottaviano (special guest Giovanni Falzone), Vostok Project, Kaìlia, BandAdriatica - Odissea, Alex Semprevivo - Art of the Messengers, Alex’s Hand - Hungarian Spa, La Répétition - Orchestra Senza Confini.
    
    In 2019 I decide to leave my job as a teacher to concentrate only on myself, and I record the first album in my name: BASES, in trio with my “brothers”, with whom I started to play music as a professional when my career begun.
  </div>
</div>