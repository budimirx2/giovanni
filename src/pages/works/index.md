---
templateKey: 'works-page'
path: /works
title: Works
image: /img/GiovanniChirico-08.jpg
# description: >-
#   Kaldi is the ultimate spot for coffee lovers who want to learn about their
#   java’s origin and support the farmers that grew it. We take coffee production,
#   roasting and brewing seriously and we’re glad to pass that knowledge to
#   anyone.
# intro:
#   blurbs:
#     - image: /img/coffee.png
#       text: >
#         We sell green and roasted coffee beans that are sourced directly from
#         independent farmers and farm cooperatives. We’re proud to offer a
#         variety of coffee beans grown with great care for the environment and
#         local communities. Check our post or contact us directly for current
#         availability.
#     - image: /img/coffee-gear.png
#       text: >
#         We offer a small, but carefully curated selection of brewing gear and
#         tools for every taste and experience level. No matter if you roast your
#         own beans or just bought your first french press, you’ll find a gadget
#         to fall in love with in our shop.
#     - image: /img/tutorials.png
#       text: >
#         Love a great cup of coffee, but never knew how to make one? Bought a
#         fancy new Chemex but have no clue how to use it? Don't worry, we’re here
#         to help. You can schedule a custom 1-on-1 consultation with our baristas
#         to learn anything you want to know about coffee roasting and brewing.
#         Email us or call the store for details.
#     - image: /img/meeting-space.png
#       text: >
#         We believe that good coffee has the power to bring people together.
#         That’s why we decided to turn a corner of our shop into a cozy meeting
#         space where you can hang out with fellow coffee lovers and learn about
#         coffee making techniques. All of the artwork on display there is for
#         sale. The full price you pay goes to the artist.
#   heading: What we offer
#   description: >
#     Kaldi is the ultimate spot for coffee lovers who want to learn about their
#     java’s origin and support the farmers that grew it. We take coffee
#     production, roasting and brewing seriously and we’re glad to pass that
#     knowledge to anyone. This is an edit via identity...
# main:
#   heading: Great coffee with no compromises
#   description: >
#     We hold our coffee to the highest standards from the shrub to the cup.
#     That’s why we’re meticulous and transparent about each step of the coffee’s
#     journey. We personally visit each farm to make sure the conditions are
#     optimal for the plants, farmers and the local environment.
#   image1:
#     alt: A close-up of a paper filter filled with ground coffee
#     image: /img/products-grid3.jpg
#   image2:
#     alt: A green cup of a coffee on a wooden table
#     image: /img/products-grid2.jpg
#   image3:
#     alt: Coffee beans
#     image: /img/products-grid1.jpg
# testimonials:
#   - author: Elisabeth Kaurismäki
#     quote: >-
#       The first time I tried Kaldi’s coffee, I couldn’t even believe that was
#       the same thing I’ve been drinking every morning.
#   - author: Philipp Trommler
#     quote: >-
#       Kaldi is the place to go if you want the best quality coffee. I love their
#       stance on empowering farmers and transparency.
# full_image: /img/products-full-width.jpg
# pricing:
#   heading: Monthly subscriptions
#   description: >-
#     We make it easy to make great coffee a part of your life. Choose one of our
#     monthly subscription plans to receive great coffee at your doorstep each
#     month. Contact us about more details and payment info.
#   plans:
#     - description: Perfect for the drinker who likes to enjoy 1-2 cups per day.
#       items:
#         - 3 lbs of coffee per month
#         - Green or roasted beans"
#         - One or two varieties of beans"
#       plan: Small
#       price: '50'
#     - description: 'Great for avid drinkers, java-loving couples and bigger crowds'
#       items:
#         - 6 lbs of coffee per month
#         - Green or roasted beans
#         - Up to 4 different varieties of beans
#       plan: Big
#       price: '80'
#     - description: Want a few tiny batches from different varieties? Try our custom plan
#       items:
#         - Whatever you need
#         - Green or roasted beans
#         - Unlimited varieties
#       plan: Custom
#       price: '??'
---
<div class="columns">
  <div class="column is-6">
    <strong>ENGLISH</strong>
      BASES is the name of the first album of Giovanni Chirico.
      
      Modern society is constantly evolving, so strongly and fastly that sometimes it becomes very easy to get confused and misunderstand what the fundamental principles of life are, at least for me.
      
      So you make your choices, right or wrong, good or bad, you try to determine yourself, you're happy when you reach your goal, you get angry when you can't, especially if you think it doesn't depend on you. You are happy, you are sad. You leave what could be the love of your life, you leave your stable job. You are sure of nothing.
      
      For this reason I felt necessary to COME BACK, to come back to the BASES. The bases on which
      
      I was born as a musician and as a person. My small town, periphery of societies, uses, thoughts, not far from and not near the city. My old friends Donato and Davide, brothers for me, musicians of Bases, also from the periphery. My family, despite it was and it’s still difficult to build my social position as a “man” because of what I decided to do in my life. Music, which in the periphery still keeps a magic bond: sharing.
      
      Then I thought that the best way for me to start being serious for my life and for my ethics was to clearly show my identity, my bases, and to play a music not just for amateurs, but especially for all the people who listen to music occasionally, for the uncle who was a DJ when he was young, for the parents who listen to the radio while traveling to visit their sons at the university in the North, for the cousin who plays football, for a nephew crazy for the video games, for your friends who like dancing in the disco. To those who are alone, to those who are curious, to those who are apathetic, to those who have no time to waste.
      
      All this with a small peculiarity: the set up.
      Baritone sax, acoustic guitar, drums.
      
      At first impression it might seem that some fundamental instruments are missing. Certainly is not missing the energy, thanks to which we made the impossible (when it seemed impossible in our area to seriously study music, to make a band). This energy pushed me to leave the engineering studies and to buy "again" a saxophone, it pushed Donato to put a town upside down just to play together, which pushed Davide to load a complete drum set on a scooter and go for rehearsals with any weather.
      
      The real bases of music for me are not the instruments. The magic is created with the right intention and with sharing.
      
      Good listening.
  </div>

  <div class="column is-6">
    <strong>ITALIAN</strong>
      BASES is the name of the first album of Giovanni Chirico.
      
      Modern society is constantly evolving, so strongly and fastly that sometimes it becomes very easy to get confused and misunderstand what the fundamental principles of life are, at least for me.
      
      So you make your choices, right or wrong, good or bad, you try to determine yourself, you're happy when you reach your goal, you get angry when you can't, especially if you think it doesn't depend on you. You are happy, you are sad. You leave what could be the love of your life, you leave your stable job. You are sure of nothing.
      
      For this reason I felt necessary to COME BACK, to come back to the BASES. The bases on which
      
      I was born as a musician and as a person. My small town, periphery of societies, uses, thoughts, not far from and not near the city. My old friends Donato and Davide, brothers for me, musicians of Bases, also from the periphery. My family, despite it was and it’s still difficult to build my social position as a “man” because of what I decided to do in my life. Music, which in the periphery still keeps a magic bond: sharing.
      
      Then I thought that the best way for me to start being serious for my life and for my ethics was to clearly show my identity, my bases, and to play a music not just for amateurs, but especially for all the people who listen to music occasionally, for the uncle who was a DJ when he was young, for the parents who listen to the radio while traveling to visit their sons at the university in the North, for the cousin who plays football, for a nephew crazy for the video games, for your friends who like dancing in the disco. To those who are alone, to those who are curious, to those who are apathetic, to those who have no time to waste.
      
      All this with a small peculiarity: the set up.
      Baritone sax, acoustic guitar, drums.
      
      At first impression it might seem that some fundamental instruments are missing. Certainly is not missing the energy, thanks to which we made the impossible (when it seemed impossible in our area to seriously study music, to make a band). This energy pushed me to leave the engineering studies and to buy "again" a saxophone, it pushed Donato to put a town upside down just to play together, which pushed Davide to load a complete drum set on a scooter and go for rehearsals with any weather.
      
      The real bases of music for me are not the instruments. The magic is created with the right intention and with sharing.
      
      Good listening.
  </div>
</div>