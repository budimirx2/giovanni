---
templateKey: index-page
title: Great coffee with a conscience
image: /img/home-jumbotron.jpg
heading: Great coffee with a conscience
subheading: Support sustainable farming while enjoying a cup
mainpitch:
  title: Why Kaldi
  description: >
    Kaldi is the coffee store for everyone who believes that great coffee
    shouldn't just taste good, it should do good too. We source all of our beans
    directly from small scale sustainable farmers and make sure part of the
    profits are reinvested in their communities.
description: >-
  Kaldi is the ultimate spot for coffee lovers who want to learn about their
  java’s origin and support the farmers that grew it. We take coffee production,
  roasting and brewing seriously and we’re glad to pass that knowledge to
  anyone.
intro:
  blurbs:
    - image: /img/coffee.png
      text: >
        We sell green and roasted coffee beans that are sourced directly from
        independent farmers and farm cooperatives. We’re proud to offer a
        variety of coffee beans grown with great care for the environment and
        local communities. Check our post or contact us directly for current
        availability.
    - image: /img/coffee-gear.png
      text: >
        We offer a small, but carefully curated selection of brewing gear and
        tools for every taste and experience level. No matter if you roast your
        own beans or just bought your first french press, you’ll find a gadget
        to fall in love with in our shop.
    - image: /img/tutorials.png
      text: >
        Love a great cup of coffee, but never knew how to make one? Bought a
        fancy new Chemex but have no clue how to use it? Don't worry, we’re here
        to help. You can schedule a custom 1-on-1 consultation with our baristas
        to learn anything you want to know about coffee roasting and brewing.
        Email us or call the store for details.
    - image: /img/meeting-space.png
      text: >
        We believe that good coffee has the power to bring people together.
        That’s why we decided to turn a corner of our shop into a cozy meeting
        space where you can hang out with fellow coffee lovers and learn about
        coffee making techniques. All of the artwork on display there is for
        sale. The full price you pay goes to the artist.
  heading: What we offer
  description: >
    Kaldi is the ultimate spot for coffee lovers who want to learn about their
    java’s origin and support the farmers that grew it. We take coffee
    production, roasting and brewing seriously and we’re glad to pass that
    knowledge to anyone. This is an edit via identity...
main:
  heading: Great coffee with no compromises
  description: >
    We hold our coffee to the highest standards from the shrub to the cup.
    That’s why we’re meticulous and transparent about each step of the coffee’s
    journey. We personally visit each farm to make sure the conditions are
    optimal for the plants, farmers and the local environment.
  image1:
    alt: A close-up of a paper filter filled with ground coffee
    image: /img/products-grid3.jpg
  image2:
    alt: A green cup of a coffee on a wooden table
    image: /img/products-grid2.jpg
  image3:
    alt: Coffee beans
    image: /img/products-grid1.jpg
---
<!-- <iframe src="https://calendar.google.com/calendar/embed?height=600&amp;wkst=2&amp;bgcolor=%23ffffff&amp;ctz=Europe%2FBelgrade&amp;src=Mm5xaTlrOG1pbWZjdHZpcm10YTQ4cGVzcTBAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ&amp;color=%234285F4&amp;showTitle=0&amp;showNav=0&amp;showDate=0&amp;showPrint=0&amp;showTabs=0&amp;showCalendars=0&amp;showTz=0&amp;mode=AGENDA" style="border-width:0" width="800" height="600" frameborder="0" scrolling="no"></iframe> -->
<iframe width="100%" height="600" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/users/368483009&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe>