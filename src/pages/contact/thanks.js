import React from 'react'
import Layout from '../../components/Layout'

export default () => (
  <Layout>
    <section className="section">
      <div className="container">
        <div className="content">
          <h1>Thank you!</h1>
          <p>Your email is on the way and we'll give our best to reply soon!</p>
        </div>
      </div>
    </section>
  </Layout>
)
